// Function to get the make and model of the last car in the inventory
function getLastCarMakeAndModel(inventory) {
    const lastCar = inventory[inventory.length - 1];

    const make = lastCar.car_make;
    const model = lastCar.car_model;

    return { make, model };
}

// Exporting the function
module.exports = getLastCarMakeAndModel;