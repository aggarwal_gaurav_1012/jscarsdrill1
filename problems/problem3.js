function sortCarModelsAlphabetically(inventory) {
    // Extract car models into a separate array
    const carModels = [];
    for (let i = 0; i < inventory.length; i++) {
        carModels.push(inventory[i].car_model);
    }

    // Sort car models alphabetically
    carModels.sort();

    // Log sorted car models
    console.log(carModels);

    // Return sorted car models array
    return carModels;
}
module.exports = sortCarModelsAlphabetically;