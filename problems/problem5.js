function countAndFilterCarsBeforeYear2000(inventory) {
    let olderCars = [];
    let count = 0;

    for (let i = 0; i < inventory.length; i++) {
        if (inventory[i].car_year < 2000) {
            olderCars.push(inventory[i]);
            count++;
        }
    }

    console.log("Number of cars made before the year 2000:", count);
    return olderCars;
}
module.exports = countAndFilterCarsBeforeYear2000;