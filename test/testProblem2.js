const getLastCarMakeAndModel = require('../problems/problem2.js');

// Test function
function testProblem2() {
    const inventory = require('../data.js');

    const lastCar = getLastCarMakeAndModel(inventory);
    console.log(`Last car is a ${lastCar.make} ${lastCar.model}`);
}

// Calling the test function
testProblem2();