const countAndFilterCarsBeforeYear2000 = require('../problems/problem5');

// Import the inventory array
const inventory = require('../data.js');

// Test the function
const olderCars = countAndFilterCarsBeforeYear2000(inventory);
console.log("Array of older cars:", olderCars);